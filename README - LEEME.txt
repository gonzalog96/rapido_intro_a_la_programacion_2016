# R-A-P-I-D-O #

El jugador posee 60 segundos para adivinar la palabra correcta, con las letras que van cayendo[...]
[...] a medida que pasa el tiempo. 

Al escribir la palabra y presionar ENTER, se escuchara un sonido caracteristico, identificando[...]
[...] si la misma es correcta o no. Si es correcta, sumara puntos y sera borrada de la pantalla[...]
[...] para dar origen a una nueva.
A medida que pasa el tiempo, se brinda al jugador las letras iniciales de la palabra como metodo de ayuda.

La palabra se va generando de manera automatica a medida que baja, hasta llegar a la linea blanca del piso[...]
[...] momento en el cual, es eliminada para dar origen a una nueva. 
A medida que transcurre el tiempo, las letras caen mas rapidamente, lo que implica un nivel mayor de[...]
[...] concentracion por parte del usuario. No obstante, posee mayor cantidad de letras de ayuda.

#############################################################
# UNGS - INTRODUCCION A LA PROGRAMACION - 1ER SEMESTRE 2016 #
#       DOCENTES: SANTIAGO MONTIEL | LEONOR GUTIERREZ       #
#                   ALUMNO: GONZALO GODOY.                  #
#############################################################